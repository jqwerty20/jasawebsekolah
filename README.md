# TENTANG JASAWEBSEKOLAH.ID #

[jasawebsekolah.id](http://jasawebsekolah.id) adalah layanan jasa pembuatan web sekolah dengan biaya mulai dari 100 ribuan per bulan.

### PAKET ###

* Web Sekolah Basic
* Web Sekolah Standard
* Web Sekolah Premium

### Modul Tambahan ###

* [Modul Kelulusan & Pengumuman Hasil Belajar](http://jasawebsekolah.id/modul-penilaian.php)
* [Modul e-Learning](http://jasawebsekolah.id/modul-elearning.php)
* [Jasa Integrasi Google Suite & Microsoft 365](http://jasawebsekolah.id/modul-gsuite-office.php)